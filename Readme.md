# Lista de Universidades de Colombia

## Motivación
Este proyecto fué realizado con fines practicos durante el curso de Programación Web con el profesor [Marco Antonio Adarme](https://gitlab.com/madarme "Marco Antonio Adarme") de la Universidad Francisco de Paula Santander.

## Tecnologias
Para realización de este proyecto utilizamos [Bootstrap5](https://getbootstrap.com/ "Bootstrap5") para los estilos de la pagina y [Javascript](https://developer.mozilla.org/es/docs/Web/JavaScript "Javascript") para definir el comportamiento y la funcionalidad de la misma.

## Screenshot
![image](/uploads/c3b79a1a803025d8759c519d85d4aeec/image.png)

## Creditos
Author: Jeison Damian Murillo Sepulveda. Cod_ufps: 1151979